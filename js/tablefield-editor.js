(function ($, Drupal, window, document) {

  Drupal.behaviors.tablefieldEditor = {
    attach: function (context, settings) {
      var format = drupalSettings.editor.formats['basic_html'];
      settings.tablefield_ids.forEach(function(id) {
        var $textareas = $('[data-drupal-selector= ' + id + ']');

        $textareas.once('tablefield-editor').each(function() {
          Drupal.editorAttach(this, format);
        });
      });
    }
  }

})(jQuery, Drupal, this, this.document);
